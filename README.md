# APP-Projekt Forest Magic

Bei dieser Applikation handelt es sich um eine Implementation
des Spiels "Forest Magic". Das Programm entstand im Rahmen
des Moduls "Allgemeines Programmierpraktikum".

Ersteller dieses Projekts sind Daniel Wiedenmeier, Egi Brako und
Lasslo Steininger. Dominick Leppich stand uns als Tutor zur Seite.

## Entpacken

Um das Programm zu entpacken, verwenden sie den Befehl

    $ tar xf app-projekt-forest-magic.tar

in dem Verzeichnis, in dem sich das Archiv befindet. Es sollte ein
Verzeichnis namens `app-projekt-forest-magic` entstehen.

## Übersetzen

Begeben sie sich in das Verzeichnis `app-projekt-forest-magic`
und verwenden sie den Befehl

    $ ant

um die Quelldateien in ein lauffähiges Programm zu übersetzen und
die Dokumentation zu erzeugen. Die Verzeichnisse `doc`, `build` und
`dist` sollten nun vorliegen. `doc` enthält die Dokumentation
für die API, `build` die übersetzten Klassendateien und `dist`
das jar-Archiv des Programms.

## Programm starten

Um das Programm auszuführen, begeben sie sich in das Verzeichnis `dist`.
Dort sollte ein Datei namens `ForestMagic.jar` vorliegen.
Verwenden sie den Befehl

    $ java -jar ForestMagic.jar

um das Programm zu starten. Damit das Spiel spielbar ist, werden
allerdings noch einige Kommandozeilenargumente benötigt.
Beim starten des Spiels stehen ihnen verschieden Argumente zur Verfügung,
mit denen sie das Spiel konfigurieren können.

******

**`--config`** (Pflicht)

Auf das Argument `--config` muss ein Pfad zu einer Datei folgen, die eine
gültige Spielbrettkonfiguration beschreibt. Im Verzeichnis
`gameconfigurations` liegen die Dateien `three_players.cfg`,
`four_players.cfg` und `five_players.cfg`. Diese Dateien enthalten
gültige Spielbrettkonfigurationen für jeweils 3, 4 oder 5 Spieler*innen.
Die Spielbrettkonfiguration bestimmt mit wie vielen Spielern
gespielt wird.

******

**`--playerNames`** (pflicht)

Auf das Argument `--playerNames` folgen die Namen für die Spieler.
Die Anzahl der Namen muss mit der Anzahl der Spieler übereinstimmen.
Die Reihenfolge der Namen entspricht der Zugreihenfolge der Spieler.

******

**`--playerTypes`** (pflicht)

Auf das Argument `--playerTypes` folgen die Typen für die Spieler.
Zur Auswahl stehen die Optionen `HUMAN` für menschliche Spieler
und `RANDOM_AI` für eine Zufalls-KI.
Die Anzahl der Typen muss mit der Anzahl der Spieler übereinstimmen.
Die Reihenfolge der Typen entspricht der Zugreihenfolge der Spieler,
dementsprechend ist der erste Spieler der Goblin.

******

**`--delay`** (optional)

Die Zahl nach `--delay` bestimmt wieviele Millisekunden Verzögerung
zwischen zwei Zügen besteht. Standardmäßig liegt dieser Wert bei 500.

******

**`--help`** (optional)

Dieses Argument gibt eine Hilfe zur Verwendung der Argumente aus.
