package forestmagicpp.game;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import forestmagicpp.preset.ArgumentParser;
import forestmagicpp.preset.GameConfiguration;
import forestmagicpp.preset.GameStatus;
import forestmagicpp.preset.GameView;
import forestmagicpp.preset.MoveStatus;
import forestmagicpp.preset.Player;
import forestmagicpp.preset.PlayerRole;
import forestmagicpp.preset.PlayerType;
import forestmagicpp.preset.customgraphics.GView;
import forestmagicpp.preset.move.Move;
import forestmagicpp.preset.move.PlayerMove;
import forestmagicpp.player.HumanPlayer;
import forestmagicpp.player.AIPlayer;

/**
 * Enthält und erstellt alle Spielrelevanten Komponenten und steuert den
 * Spielablauf. Wenn die Methoden notifyMove(), requestStatus() und notifyStatus()
 * nicht in der richtigen Reihenfolge verwendet werden, hat dies Exceptions zur Folge.
 * 
 * @author Lasslo Steininger
 */
public class Game {
    /** Verwertet die Kommandozeilenargumente */
    private ArgumentParser argParser;
    
    /** Spielkonfiguration */
    private GameConfiguration gameConfig;

    /** Referenz auf die Hauptspielgrafik */
    private GameView gameView = new GView();

    /** Referenz auf die Goblingrafik */
    private GameView goblinView = new GView();

    /** Liste aller Spieler */
    private List<Player> players = new ArrayList<>(0);

    /** Spielbrett des Hauptspiels */
    private GameBoard gameBoard;

    /** Status des aktuellen Spiels */
    private GameStatus gameStatus;

    /**
     * Erzeugt ein Spiel mit den Kommandozeilenargumenten
     * für die Spielkonfiguration.
     * 
     * @param args Kommandozeilenargumente
     * @throws Exception
     */
    private Game(String[] args) throws Exception {
        argParser = new ArgumentParser(args);
        gameConfig = new GameConfiguration(argParser.getGameConfiguration());

        // Befülle die Liste der Spieler
        for (int i = 0; i < argParser.getPlayerNames().size(); i++) {
            // Jeder Spieler ist HUMAN oder eine RANDOM_AI
            if (argParser.getPlayerTypes().get(i).equals(PlayerType.HUMAN))
                players.add(new HumanPlayer(argParser.getPlayerNames().get(i), PlayerType.HUMAN, gameView, goblinView));
            else
                players.add(new AIPlayer(argParser.getPlayerNames().get(i), PlayerType.RANDOM_AI));

            // Initialisiere jeden Spieler
            // Spieler 0 ist Goblin
            players.get(i).init(gameConfig, i == 0 ? PlayerRole.GOBLIN : PlayerRole.FAIRY, i);
        }

        BoardGenerator boardGenerator = new BoardGenerator(argParser);
        gameBoard = boardGenerator.getGameBoard();
        gameView.setViewer(gameBoard.viewer());
        gameView.setTitle("Forest Magic");
        gameStatus = GameStatus.GOBLIN_WINS;
    }

    /**
     * Hauptklasse, erzeugt mit Kommandozeilenargumenten ein neues Spiel
     * und startet dieses.
     * 
     * @param args Kommandozeilenargumente
     */
    public static void main(String[] args) {
        try {
            // Erzeuge neues Spiel
            Game game = new Game(args);
            
            // Starte Spiel
            game.play();
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    /**
     * Startet das Spiel. Erfragt der Reihe nach Züge von den Spielern und überprüft
     * den Spielstatus.
     */
    private void play() {
        try {
            Thread.sleep(2000);

            // Solange das Spiel nicht beendet ist und kein Fehler auftritt
            while (gameBoard.isGameRunning()) {
                // Delay 
                Thread.sleep(argParser.getDelay());

                // Erfrage Move bei aktivem Spieler
                PlayerMove playerMove = players.get(gameBoard.getActivePlayerNumber()).requestMove();

                // Teile Move allen anderen Spielern mit
                notifyMove(playerMove);

                // Aktualisiere das Spielbrett und Grafik
                gameBoard.update(playerMove.getMove());
                gameView.update(playerMove.getMove());

                // Ermittle den Gesamt-MoveStatus
                MoveStatus moveStatus = requestStatus();

                // Teile MoveStatus allen Spielern mit
                notifyStatus(moveStatus);

                switch (moveStatus) {
                    case OK:
                        // Nichts passiert. Das Spiel geht weiter.
                        break;
                
                    case VERIFY:
                        // Ein Spieler behauptet, das Spiel sei vorzeitig vorbei.
                        // Das Spiel wird beendet. Es wird vom Sieg der Feen ausgegangen.
                        gameStatus = GameStatus.FAIRIES_WIN;
                        gameBoard.endGame();
                        break;

                    case ILLEGAL:
                        // Ein Spieler behauptet der Spielstatus sei unzulässig.
                        // Das Spiel wird beendet.
                        gameStatus = GameStatus.ILLEGAL;
                        gameBoard.endGame();
                        break;
                }
            }
        } catch (Exception e) {
            // Ein Fehler ist aufgetreten. Das Spiel wird abgebrochen
            System.out.println(e);
            gameStatus = GameStatus.ILLEGAL;
        }

        // Das Spiel wird von den Feen verifiziert und das Ergebnis angezeigt
        verify();
        gameView.showStatus(gameStatus);

        // Falls der Goblin HUMAN ist, wird auch auf seiner Grafik das Ergebnis angezeigt
        if (players.get(0) instanceof HumanPlayer)
            goblinView.showStatus(gameStatus);
    }

    /**
     * Teilt allen Mitspielern des aktiven Spielers den gemachten Zug mit.
     * Methode muss nach notifyStatus() aufgerufen werden oder
     * noch bevor requestStatus() und notifyStatus() überhaupt verwendet wurden.
     * 
     * @param playerMove PlayerMove des aktiven Spielers
     * @throws Exception Falls ein Spieler den Move nicht akzeptiert oder
     * die Reihenfolge der Methodenaufrufe fehlerhaft ist
     */
    private void notifyMove(PlayerMove playerMove) throws Exception {
        for (int i = 0; i < gameBoard.getNumberOfFairies() + 1; i++)
            // Dem aktiven Spieler wird der Move nicht erneut mitgeteilt
            if (i != gameBoard.getActivePlayerNumber())
                players.get(i).notifyMove(playerMove);
    }

    /**
     * Erfragt von allen Spielern welchen MoveStatus der letzte Zug
     * ausgelöst hat. Bestimmt aus allen Status den Gesamtstatus.
     * Methode muss nach notifyMove() verwendet werden.
     * 
     * @return Gesamtstatus
     * @throws Exception Falls ein Spieler noch keinen Status hat oder
     * die Reihenfolge der Methodenaufrufe fehlerhaft ist
     */
    private MoveStatus requestStatus() throws Exception {
        ArrayList<MoveStatus> playerStatus = new ArrayList<>(0);

        // Erfrage alle Status
        for (int i = 0; i < gameBoard.getNumberOfFairies() + 1; i++)
            playerStatus.add(players.get(i).requestStatus());

        // Ermittle Gesamtstatus
        MoveStatus status = MoveStatus.OK;
        Iterator<MoveStatus> iterator = playerStatus.iterator();

        while (iterator.hasNext()) {
            switch (iterator.next()) {
                case OK:
                    // Ist ein Status OK ändert das den Gesamtstatus nicht
                    // Sind alle Status OK, ist der Gesamtstatus OK
                    break;
            
                case VERIFY:
                    // Ist ein Status VERIFY, ist der Gesamtstatus VERIFY,
                    // es sei denn ein Status ist ILLEGAL
                    if (!status.equals(MoveStatus.ILLEGAL))
                        status = MoveStatus.VERIFY;
                
                    break;
                
                case ILLEGAL:
                    // Ist ein Status ILLEGAL, ist der Gesamtstatus ILLEGAL
                    status = MoveStatus.ILLEGAL;
                    break;
            }
        }

        return status;
    }

    /**
     * Teilt allen Spielern den, durch den letzten Zug ausgelösten Gesamtstatus mit.
     * Methode muss nach requestStatus() verwendet werden.
     * 
     * @param status MoveStatus Gesamtstatus
     * @throws Exception Falls ein Spieler den Status ILLEGAL hat oder
     * die Reihenfolge der Methodenaufrufe fehlerhaft ist
     */
    private void notifyStatus(MoveStatus status) throws Exception {
        for (int i = 0; i < gameBoard.getNumberOfFairies() + 1; i++)
            players.get(i).notifyStatus(status);
    }

    /**
     * Startet die Verifikation des Spiels. Erfragt alle Züge und Salts vom Goblin
     * und gibt diese an die Feen damit diese das Spiel verifizieren können.
     * Die Methode bestimmt den resultierenden Spielstatus.
     * Darf nur verwendet werden, wenn das Spiel vorbei ist.
     */
    private void verify() {
        List<Move> goblinMoves = null;
        List<String> salts = null;

        try {
            // Erfrage die Moves und Salts des Goblins
            goblinMoves = players.get(0).requestAllMoves();
            salts = players.get(0).requestAllSalts();

            // Starte Verifizierung bei jeder Fee
            for (int i = 1; i < gameBoard.getNumberOfFairies() + 1; i++)
                players.get(i).verifyGame(gameStatus, goblinMoves, salts);
        } catch (IllegalStateException e) {
            // Der Goblin hat geschummelt
            System.out.println(e);
            gameStatus = GameStatus.FAIRIES_WIN;
        } catch (Exception e) {
            // Eine Methode wurde zu einem ungültigen Zeitpunkt aufgerufen
            // oder ein Fehler ist aufgetreten
            System.out.println(e);
            gameStatus = GameStatus.ILLEGAL;
        }
    }
}
