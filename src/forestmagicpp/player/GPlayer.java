package forestmagicpp.player;

import java.util.ArrayList;
import java.util.List;

import forestmagicpp.preset.*;
import forestmagicpp.game.GameBoard;
import forestmagicpp.preset.move.*;

/**
 * Spielerklasse des Forest Magic Spiels.
 *
 * @author Egi Brako, Lasslo Steininger
 */
public abstract class GPlayer implements Player {
    /** Spielkonfiguration */
    GameConfiguration gameConfig;

    /** Rolle des Spielers */
    PlayerRole role;

    /** Reihenfolge des Spielers */
    int turn;

    /** Set aller durchgeführten Züge */
    ArrayList<Move> moves = new ArrayList<>(0);

    /** Game Viewer von der Feen **/
    GameView fairyView;

    /** Game Viewer vom Goblin **/
    GameView goblinView;

    /**Eigene Gameviewer für jede spieler**/
    GameView gameView;

    /** Move status. Ändert sich von einem illegalen Spielzug**/
    MoveStatus moveStatus = MoveStatus.OK;

    /** Name des Spielers **/
    String name;

    /** Typ des Spielers **/
    PlayerType type;

    /** Salts vom Goblin Spieler **/
    List<String> salts = new ArrayList<>(0);

    /** Lokales Spielbrett, das von allen Spielern verwendet wird **/
    GameBoard gameBoard;

    /** Spieleranzahl **/
    int numberOfPlayers;

    /** List mit den von Goblin uebergebene hashwerte**/
    List<String> goblinHashes = new ArrayList<>(0);

    /** Wird zum Überprüfen der Reihenfolge der Aufrufe der Spielermethode verwendet **/
    PlayerMethods previousMethod = PlayerMethods.START;

    /**
    * Erzeugt diesen Spieler.
    * @param name Name des Spielers
    * @param type Typ des Spielers
    * @param fairyView Die Game Viewer von Feen (wird verwendet wenn mind. eine Fee ein HUMAN Spieler ist)
    * @param goblinView Die Goblin Game Viewer (wird verwendet wenn der Goblin ein HUMAN Spieler ist)
    */
    public GPlayer(String name, PlayerType type, GameView fairyView, GameView goblinView) {
        this.name = name;
        this.type = type;
        this.fairyView = fairyView;
        this.goblinView = goblinView;
    } 

    @Override
    public abstract void init(GameConfiguration gameConfiguration, PlayerRole role, int turn) throws Exception;

    @Override
    public abstract PlayerMove requestMove() throws Exception;

    @Override
    public abstract void notifyMove(PlayerMove otherMove) throws Exception; 

    @Override
    public String name() throws Exception {
        // name ist null/leer
	    if(name == null || name.equals(""))
	    	throw new RuntimeException("Player name is null or empty");

        return name;
    }

    @Override
    public void notifyStatus(MoveStatus status) throws Exception {
        checkOrder(PlayerMethods.REQUEST_STATUS);
        previousMethod = PlayerMethods.NOTIFY_STATUS;

        // Wenn der lokale Status ILLEGAL und der uebergebene Status nicht ILLEGAL ist, wird eine Exception geworfen
        if (this.moveStatus.equals(MoveStatus.ILLEGAL) && !status.equals(MoveStatus.ILLEGAL))
            throw new Exception(name() + " 's ILLEGAL status does not match the overall status");

        // Ende das Spiel falls der uebergebene Status nicht OK ist
        if (status.equals(MoveStatus.ILLEGAL) || status.equals(MoveStatus.VERIFY))
            gameBoard.endGame();

        this.moveStatus = status;
        return;
    }

    @Override
    public MoveStatus requestStatus() throws Exception {
        checkOrder(PlayerMethods.REQUEST_MOVE, PlayerMethods.NOTIFY_MOVE);
        previousMethod = PlayerMethods.REQUEST_STATUS;

        if (moveStatus == null)
            throw new RuntimeException("status is null");

        return moveStatus;
    }

    @Override
    public List<Move> requestAllMoves() throws Exception {
        previousMethod = PlayerMethods.REQUEST_ALL_MOVES;

        if(moveStatus != MoveStatus.VERIFY && moves.size() < gameConfig.getMaxNumberOfRounds())
            throw new RuntimeException("method cannot be called before the game has finished!");
        if (role != PlayerRole.GOBLIN)
            throw new RuntimeException("method cannot be called by a fairy");
        if (moves == null || moves.size() == 0)
            throw new RuntimeException("moves list is null or empty");

        return moves;
    }

    @Override
    public List<String> requestAllSalts() throws Exception {
        checkOrder(PlayerMethods.REQUEST_ALL_MOVES);
        previousMethod = PlayerMethods.REQUEST_ALL_SALTS;

        if(moveStatus != MoveStatus.VERIFY && moves.size() < gameConfig.getMaxNumberOfRounds())
            throw new RuntimeException("method cannot be called outside of verification phase!");
        if (role != PlayerRole.GOBLIN)
            throw new RuntimeException("method cannot be called by a fairy");
        if (salts == null || salts.size() == 0)
            throw new RuntimeException("salts list is null or empty");

        return salts;
    }

    @Override
    public void verifyGame(GameStatus status, List<Move> moves, List<String> salts) throws Exception {
        previousMethod = PlayerMethods.VERIFY_GAME;
        GameStatus gameStatus;
        // falls der Zug Status VERIFY ist, dann haben die Feen der Goblin getroffen
        if (moveStatus == MoveStatus.VERIFY)
            gameStatus = GameStatus.FAIRIES_WIN;
        // falls der Zug Status ILLEGAL ist, dann ist das ganze Spiel ILLEGAL
        else if (moveStatus == MoveStatus.ILLEGAL)
            gameStatus = GameStatus.ILLEGAL;
        // falls nicht die erste beide, dann ist der Goblin den Feen entkommen, und er hat gewonnen.
        else
            gameStatus = GameStatus.GOBLIN_WINS;

        //erzeuge MoveHasher fuer die Ueberpruefung
        MoveHasher fairyHasher = new MoveHasher();
        String fairyGeneratedHash;

        //methode kann nicht vom Goblin aufgerufen werden.
        if (role != PlayerRole.FAIRY)
            throw new RuntimeException("method cannot be called by goblin");

        // in einem wahren Spiel, sollen diese uebereinstimmen
        if (moves.size() != salts.size())
            throw new RuntimeException("lengths of moves and salts are not equal");

        // iteriere durch alle uebergebene Spielzuege
        for (int i = 0; i < moves.size(); i++) {
            //generiere die Hashwerte
            fairyGeneratedHash = fairyHasher.calculateMoveHash(salts.get(i), moves.get(i));

            // pruefe ob die zwei Hashwerte gleich sind
            if (!fairyGeneratedHash.equals(goblinHashes.get(i)))
                gameStatus = GameStatus.FAIRIES_WIN;

            // falls die zwei moves Listen(die vom Goblin und die von der Fee) verschiedenen Groessen haben, soll der letzte Zug nicht ueberprueft werden
            if (i < this.moves.size() && 
                // pruefe basierend von zwei verschiedenen Faellen, ob der Goblin in einer Runde gefunden wurde.
               (this.moves.get(i).getSource().equals(moves.get(i).getDestination()) || 
                this.moves.get(i).getDestination().equals(moves.get(i).getDestination())))
                    gameStatus = GameStatus.FAIRIES_WIN;
        }

        if (status != gameStatus) {
            gameStatus = GameStatus.ILLEGAL; 
            throw new IllegalStateException("Der Goblin hat geschummelt");
        }
    }

    /**
     * Überprüft die Position aller Spieler im lokalen Spielbrett dieses Spielers basierend auf dem letzten ausgefuehrten Spielzug.
     * Befindet sich eine der Feen in derselben Position wie der Goblin, wird das Spiel auf die Stufe VERIFY geschickt
     * @return Einen {@link forestmagicpp.preset.MoveStatus} abhaengig von der aktuellen Position der Spieler.
     */
    MoveStatus detectLocalMoveStatus() {
        for (int i = 1; i < gameBoard.getNumberOfFairies() + 1; i++){
            if (gameBoard.getPositionOfPlayer(i).equals(gameBoard.getPositionOfPlayer(0)))
                return MoveStatus.VERIFY;
        }

        return MoveStatus.OK;
    }


    /**
     * Ueberprueft ob die Spielermethoden in den richtigen Reihenfolge aufgerufen werden, durch die Nutzung von der {@link forestmagicpp.player.PlayerMethods}.
     * @param methods Die akzeptierten vorherigen Methoden
     * @throws RuntimeException falls die Methoden nicht in der richtigen Reihenfolge aufgerufen wurden.
     */
    void checkOrder(PlayerMethods... methods) throws Exception{
        // iteriere durch die akzeptierte methoden
        for(int i = 0; i< methods.length;i++)
            //pruefe falls die vorherige methode akzeptiert ist
            if(methods[i] == previousMethod)
                return;

        throw new RuntimeException("Method called out of order. The previous called method is: " + previousMethod);
    }


    /** 
     * Methode, die sich um den Goblinzug kümmert. Es generiert die Salt und Hashwert und stellt sicher dass der Goblin Spielbrettsymbol richtig aktualisiert wird
     * @param move Der letzte ausgefuehrte Spielzug
     */
    PlayerMove updateGoblinMove(Move move){
        // generiert der Hashwert
        MoveHasher hasher = new MoveHasher();
        String salt = hasher.getSalt();
        salts.add(salt);
        String hash = hasher.calculateMoveHash(salt, move);

        // Quelle und Ziel des ausgefuehrten Zuges
        Position start;
        Position end;

        // wenn das Spiel von einer offenen zu einer verdeckten Runde wechselt, ist der Ziel unbekannt
        if (gameConfig.getVisibleRounds().contains(gameBoard.getRound() - 1))
            start = move.getSource();
        else
            start = Position.UNKNOWN;

        // wenn das Spiel von einer verdeckten zu einer offenen Runde wechselt, ist die Quelle unbekannt
        if (gameConfig.getVisibleRounds().contains(gameBoard.getRound()))
            end = move.getDestination();
        else
            end = Position.UNKNOWN;

        // Goblin uebergibt Spielzuege mit einem Hashwert
        return new HashedMove(new Move(start, end, move.getPath()), hash);
    }
}
