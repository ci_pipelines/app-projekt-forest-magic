package forestmagicpp.player;

/**
 * Diese Klasse wird verwendet, um die Reihenfolge zu steuern, in der die Spielermethoden aufgerufen werden. 
 * @author Egi Brako
 */
public enum PlayerMethods{
	START,
	REQUEST_MOVE,
	NOTIFY_MOVE,
	REQUEST_STATUS,
	NOTIFY_STATUS,
	REQUEST_ALL_MOVES,
	REQUEST_ALL_SALTS,
	VERIFY_GAME
}