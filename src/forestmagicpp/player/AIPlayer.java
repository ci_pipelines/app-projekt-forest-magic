package forestmagicpp.player;

import java.util.Random;
import java.util.Set;

import forestmagicpp.preset.*;
import forestmagicpp.game.GameBoard;
import forestmagicpp.preset.move.*;

/**
* Kuenstliche Intelligenz Spielerklasse des Forest Magic Spiels. Erbt von der abstrakte Klasse {@link forestmagicpp.player.GPlayer}.
* @author Egi Brako
*/
public class AIPlayer extends GPlayer {

    /**
     * Konstruktor fuer die Klasse, der den {@link forestmagicpp.player.GPlayer} Konstruktor aufruft, 
     * der ohne {@link forestmagicpp.preset.GameView} objekt aufgerufen wird.
     * @param name Name des Spielers
     * @param type Typ des Spielers
     */
    public AIPlayer(String name, PlayerType type) {
        super(name, type, null, null);
    } 

    @Override
    public void init(GameConfiguration gameConfiguration, PlayerRole role, int turn) throws Exception {
        gameConfig = gameConfiguration;
        this.role = role;
        this.turn = turn;
        moveStatus = MoveStatus.OK;
        previousMethod = PlayerMethods.START;
        numberOfPlayers = gameConfig.getStartPositions().size();
        gameBoard = new GameBoard(gameConfiguration);
    }

    @Override
    public PlayerMove requestMove() throws Exception {
        checkOrder(PlayerMethods.START, PlayerMethods.NOTIFY_STATUS);
        previousMethod = PlayerMethods.REQUEST_MOVE;

        // erstelle ein zufaelliger Zug
        Move move = randomMove();

        // fuege die Liste von Zuegen dieser zug hinzu
        moves.add(move);
        // aktualisiere lokales Spielbrett
        gameBoard.update(move);

        // pruefe ob dieser Zug ueberhaupt gueltig ist
        if (moveStatus == MoveStatus.OK)
            moveStatus = detectLocalMoveStatus();

        //falls der Spieler ein Goblin ist, gebe einen gehashte Spielzug zurueck
        if (role.equals(PlayerRole.GOBLIN))
            return updateGoblinMove(move);
        
        // falls der Spieler eine Fee ist, gebe einen Spielzug ohne Hash zurueck
        return new UnHashedMove(move);
    }


    /** 
     * Finde einen zufaellige Spielzug von der Liste der moegliche Spielzuege
     */
    private Move randomMove() {
        // nehmen alle moeglichen Spielzuege
        Set<Move> possibleMoves = gameBoard.getPossibleMoves();

        // generiere eine zufaellige nicht negative Zahl, kleiner als die Groeße der Liste aller moeglichen Zuege
        Random rand = new Random();
        int comparator = rand.nextInt(possibleMoves.size());
        int i = 0;

        // iteriere durch alle moegliche Spielzuege und finde den der ausgewaehlt wurde, und gebe ihn zurueck
        for (Move m : possibleMoves) {
            if (i == comparator) 
                return m;

            i++;
        }

        return null;
    }

    @Override
    public void notifyMove(PlayerMove otherMove) throws Exception {
        checkOrder(PlayerMethods.NOTIFY_STATUS, PlayerMethods.START);
        previousMethod = PlayerMethods.NOTIFY_MOVE;

        // versuche das lokale Spielbrett mit dem uebergebene Spielzug zu aktualisieren
        try {
            gameBoard.update(otherMove.getMove());

            // wenn der Zug einen Hashwert hat, fuege dieser zum Hashwerteliste hinzu
            if (otherMove.hasHash())            
                goblinHashes.add(otherMove.getHash());
        } catch (IllegalMoveException e) {
            // wenn der Zug ungueltig ist, ist das Spiel vorbei
            System.out.println(e);
            moveStatus = MoveStatus.ILLEGAL;
            throw e;
        }

        // wenn der lokale Status nicht OK ist, macht kein sinn der Status nochmal zu pruefen
        if (moveStatus == MoveStatus.OK)
            moveStatus = detectLocalMoveStatus();
    }
}