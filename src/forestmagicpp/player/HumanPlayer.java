package forestmagicpp.player;

import forestmagicpp.preset.*;
import forestmagicpp.game.GameBoard;
import forestmagicpp.preset.move.*;

/**
* Menschlicher Spielerklasse des Forest Magic Spiels. Erbt von der abstrakte Klasse {@link forestmagicpp.player.GPlayer}.
* 
* @author Egi Brako
*/
public class HumanPlayer extends GPlayer{

    /**
    * Erzeugt ein menschlicher Spieler. fuer die Klasse, der den {@link forestmagicpp.player.GPlayer#GPlayer} Konstruktor aufruft.
    * @param name Name des Spielers
    * @param type Typ des Spielers
    * @param fairyView Die Game Viewer von Feen (wird verwendet wenn mind. eine Fee ein HUMAN Spieler ist)
    * @param goblinView Die Goblin Game Viewer (wird verwendet wenn der Goblin ein HUMAN Spieler ist)
    **/
    public HumanPlayer(String name, PlayerType type, GameView fairyView, GameView goblinView) {
        super(name, type, fairyView, goblinView);
    } 

    @Override
    public void init(GameConfiguration gameConfiguration, PlayerRole role, int turn) throws Exception {
        gameConfig = gameConfiguration;
        this.role = role;
        this.turn = turn;
        moveStatus = MoveStatus.OK;
        previousMethod = PlayerMethods.START;
		numberOfPlayers = gameConfig.getStartPositions().size();
        gameBoard = new GameBoard(gameConfiguration);

        if (role.equals(PlayerRole.GOBLIN)) {
            gameView = goblinView;
            gameView.setViewer(gameBoard.viewer());
            gameView.setTitle("Goblin");
        } else
            gameView = fairyView;
    }

    @Override
    public PlayerMove requestMove() throws Exception {
        checkOrder(PlayerMethods.START, PlayerMethods.NOTIFY_STATUS);
        previousMethod = PlayerMethods.REQUEST_MOVE;

        // erstelle einen neuen move Objekt
        Move move = null;

        // falls der Fee-Spieler darf keine Zuege mehr machen, dann soll kein Zug angefordert werden
        if (gameBoard.getPossibleMoves().contains(Move.PASS))
            move = Move.PASS;
        else
            move = gameView.request();

        // fuege die Liste von Zuegen dieser zug hinzu
        moves.add(move);
        // aktualisiere lokales Spielbrett
        gameBoard.update(move);

        // pruefe ob dieser Zug ueberhaupt gueltig ist
        if (moveStatus == MoveStatus.OK)
            moveStatus = detectLocalMoveStatus();

        //falls der Spieler ein Goblin ist, gebe einen gehashte Spielzug zurueck
        if (role.equals(PlayerRole.GOBLIN)) {
            gameView.update(move);
            return updateGoblinMove(move);
        }

        // falls der Spieler eine Fee ist, gebe einen Spielzug ohne Hash zurueck
        return new UnHashedMove(move);
    }

    @Override
    public void notifyMove(PlayerMove otherMove) throws Exception {
        checkOrder(PlayerMethods.START, PlayerMethods.NOTIFY_STATUS);
        previousMethod = PlayerMethods.NOTIFY_MOVE;

        // versuche das lokale Spielbrett mit dem uebergebene Spielzug zu aktualisieren
        try {
            gameBoard.update(otherMove.getMove());

            // wenn der Zug einen Hashwert hat, fuege dieser zum Hashwerteliste hinzu
            if (otherMove.hasHash())			
                goblinHashes.add(otherMove.getHash());
        } catch (IllegalMoveException e) {
            System.out.println(e);
            moveStatus = MoveStatus.ILLEGAL;
            throw e;
        }

        // wenn der menschliche Spieler ein Goblin ist, soll seine gameView aktualisiert werden
        if (role.equals(PlayerRole.GOBLIN))
            gameView.update(otherMove.getMove());

        // wenn der lokale Status nicht OK ist, macht kein sinn der Status nochmal zu pruefen
        if (moveStatus == MoveStatus.OK)
            moveStatus = detectLocalMoveStatus();
    }
}
