package forestmagicpp.preset.customgraphics;

import forestmagicpp.preset.graphics.GContainer;

import java.util.List;

public class GPath extends GContainer {
    private List<GPathSegment> segments;

    public GPath(List<GPathSegment> segments) {
        this.segments = segments;
    }

    public void setHighlighted(boolean value) {
        for (GPathSegment s : segments) {
            s.setHighlighted(value);
        }
    }

    public List<GPathSegment> getSegments() {
        return segments;
    }
}
