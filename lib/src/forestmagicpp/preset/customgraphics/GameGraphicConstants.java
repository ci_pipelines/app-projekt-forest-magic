package forestmagicpp.preset.customgraphics;

import java.awt.*;

public class GameGraphicConstants {
    public static final Color GAME_STATUS_MESSAGE = new Color(213, 181, 3);
    public static final Color BACKGROUND_GRADIENT_LEFT = new Color(4, 140, 50);
    public static final Color BACKGROUND_GRADIENT_RIGHT = new Color(20, 186, 77);
    public static final Paint ROUND_INDICATOR_ACTIVE = new Color(2, 195, 9);
    public static final Paint ROUND_INDICATOR_INACTIVE = new Color(0, 0, 0);
    public static final Color BASE_COLOR = new Color(26, 55, 26);
    public static final Color LABEL_COLOR = new Color(255, 255, 255);

    public static final Color MOUSE_RIDE_COLOR_INACTIVE = new Color(102, 83, 51);
    public static final Color MOUSE_RIDE_COLOR_DEFAULT = new Color(221, 182, 56);
    public static final Color MOUSE_RIDE_COLOR_HIGHLIGHT = new Color(235, 211, 125);

    public static final Color CAT_RIDE_COLOR_INACTIVE = new Color(48, 64, 77);
    public static final Color CAT_RIDE_COLOR_DEFAULT = new Color(18, 99, 164);
    public static final Color CAT_RIDE_COLOR_HIGHLIGHT = new Color(151, 175, 207);

    public static final Color OWL_FLIGHT_COLOR_INACTIVE = new Color(76, 40, 44);
    public static final Color OWL_FLIGHT_COLOR_DEFAULT = new Color(189, 91, 69);
    public static final Color OWL_FLIGHT_COLOR_HIGHLIGHT = new Color(208, 153, 134);

    public static final Color SECRET_TUNNEL_COLOR_INACTIVE = new Color(56, 49, 47);
    public static final Color SECRET_TUNNEL_COLOR_DEFAULT = new Color(119, 112, 110);
    public static final Color SECRET_TUNNEL_COLOR_HIGHLIGHT = new Color(163, 165, 168);

    public static final Color[] PATH_INACTIVE_COLORS = {
            MOUSE_RIDE_COLOR_INACTIVE, CAT_RIDE_COLOR_INACTIVE, OWL_FLIGHT_COLOR_INACTIVE, SECRET_TUNNEL_COLOR_INACTIVE
    };
    public static final Color[] PATH_DEFAULT_COLORS = {
            MOUSE_RIDE_COLOR_DEFAULT, CAT_RIDE_COLOR_DEFAULT, OWL_FLIGHT_COLOR_DEFAULT, SECRET_TUNNEL_COLOR_DEFAULT
    };
    public static final Color[] PATH_HIGHLIGHT_COLORS = {
            MOUSE_RIDE_COLOR_HIGHLIGHT, CAT_RIDE_COLOR_HIGHLIGHT, OWL_FLIGHT_COLOR_HIGHLIGHT,
            SECRET_TUNNEL_COLOR_HIGHLIGHT
    };
    public static final Color GOBLIN_TOKEN_DEFAULT = new Color(129, 36, 21);
    public static final Color GOBLIN_TOKEN_HIGHLIGHT = new Color(195, 48, 23);
    public static final Color FAIRY_TOKEN_DEFAULT = new Color(2, 129, 9);
    public static final Color FAIRY_TOKEN_HIGHLIGHT = new Color(2, 195, 9);

    public static final Stroke BASIC_LINK_BACKGROUND_STROKE = new BasicStroke(0.03f);
    public static final Stroke HIDDEN_LINK_BACKGROUND_STROKE = new BasicStroke(0.045f, BasicStroke.CAP_BUTT,
            BasicStroke.JOIN_ROUND, 10.0f, new float[]{10.0f}, 0.0f);
    public static final Stroke[] LINK_BACKGROUND_STROKES = {
            BASIC_LINK_BACKGROUND_STROKE, BASIC_LINK_BACKGROUND_STROKE, BASIC_LINK_BACKGROUND_STROKE,
            HIDDEN_LINK_BACKGROUND_STROKE
    };
    public static final Stroke BASIC_LINK_STROKE = new BasicStroke(0.005f);
    public static final Stroke HIDDEN_LINK_STROKE = new BasicStroke(0.02f, BasicStroke.CAP_BUTT, BasicStroke.JOIN_ROUND,
            10.0f, new float[]{10.0f}, 0.0f);
    public static final Stroke[] LINK_STROKES = {
            BASIC_LINK_STROKE, BASIC_LINK_STROKE, BASIC_LINK_STROKE, HIDDEN_LINK_STROKE
    };

    public static final double X_CONVERSION_FACTOR = 2.0 * 16.0 / 9.0;
    public static final double X_CONVERSION_FACTOR_HALF = X_CONVERSION_FACTOR / 2.0;
    public static final double Y_CONVERSION_FACTOR = 2.0;
    public static final double Y_CONVERSION_FACTOR_HALF = Y_CONVERSION_FACTOR / 2.0;

    public static final double BOARD_DEFAULT_SCALING = 0.4;
    public static final double BOARD_MIN_SCALING = 0.25;
    public static final double BOARD_MAX_SCALING = 1.3;
}
