package forestmagicpp.preset.customgraphics;

import forestmagicpp.preset.PathType;
import forestmagicpp.preset.graphics.GContainer;
import forestmagicpp.preset.graphics.primitives.GArc;

import java.awt.*;
import java.awt.geom.Arc2D;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class GMultiField extends GContainer {
    private boolean interactable = false;
    private Map<PathType, GArc> arcs = new HashMap<>();
    private Map<PathType, Integer> defaultLevels = new HashMap<>();

    public GMultiField(final List<PathType> allTypes) {
        List<PathType> types = new ArrayList<>(allTypes);
        types.remove(PathType.SECRET_TUNNEL);

        types.stream()
             .forEach(type -> defaultLevels.put(type, 1));

        int n = types.size();

        double size = 360.0 / n;
        for (int i = 0; i < n; i++) {
            GArc arc = new GArc(90.0 + i * size, size, Arc2D.PIE);
            arc.setFillPaint(getPaintForType(types.get(i), 1));
            arcs.put(types.get(i), arc);
            add(arc);
        }

        setOnMouseEnterCallback(() -> {
            if (interactable) {
                for (Map.Entry<PathType, GArc> e : arcs.entrySet()) {
                    e.getValue()
                     .setFillPaint(getPaintForType(e.getKey(), 2));
                }
            }
        });
        setOnMouseLeaveCallback(() -> {
            for (Map.Entry<PathType, GArc> e : arcs.entrySet()) {
                e.getValue()
                 .setFillPaint(getPaintForType(e.getKey(), defaultLevels.get(e.getKey())));
            }
        });
    }

    public void setDefaultLevel(int defaultLevel) {
        arcs.keySet()
            .forEach(type -> defaultLevels.put(type, defaultLevel));
        for (Map.Entry<PathType, GArc> e : arcs.entrySet()) {
            e.getValue()
             .setFillPaint(getPaintForType(e.getKey(), defaultLevel));
        }
    }

    public void setInteractable(final boolean value) {
        this.interactable = value;
    }

    private Paint getPaintForType(PathType type, int level) {
        switch (level) {
            case 0:
                return GameGraphicConstants.PATH_INACTIVE_COLORS[type.ordinal()];
            case 1:
                return GameGraphicConstants.PATH_DEFAULT_COLORS[type.ordinal()];
            case 2:
                return GameGraphicConstants.PATH_HIGHLIGHT_COLORS[type.ordinal()];
            default:
                throw new IllegalArgumentException("No such level");
        }
    }

    public void highlightPath(PathType type) {
        if (type == PathType.SECRET_TUNNEL) {
            setDefaultLevel(1);
        } else {
            setDefaultLevel(0);
            defaultLevels.put(type, 1);
            arcs.get(type)
                .setFillPaint(getPaintForType(type, 1));
        }
    }
}
