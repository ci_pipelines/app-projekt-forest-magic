package forestmagicpp.preset.customgraphics;

import forestmagicpp.preset.PathType;
import forestmagicpp.preset.graphics.GObject;
import forestmagicpp.preset.graphics.primitives.GLine;

import java.awt.geom.Point2D;

public class GPathSegment extends GLine {
    private PathType type;
    private Point2D offsetAxis;
    private GObject background;
    private static final double OFFSET_SCALING = 0.01;

    public GPathSegment(Point2D from, Point2D to, PathType type, GObject background) {
        super(from, to);
        this.type = type;
        this.background = background;
        setStroke(GameGraphicConstants.LINK_STROKES[type.ordinal()]);
        setHighlighted(true);
        Point2D delta = new Point2D.Double(to.getX() - from.getX(), to.getY() - from.getY());
        double magnitude = Math.sqrt(delta.getX() * delta.getX() + delta.getY() * delta.getY());
        this.offsetAxis = new Point2D.Double(-delta.getY() / magnitude, delta.getX() / magnitude);
    }

    public void setHighlighted(boolean value) {
        if (value) {
            setOutlinePaint(GameGraphicConstants.PATH_DEFAULT_COLORS[type.ordinal()]);
        } else {
            setOutlinePaint(GameGraphicConstants.PATH_INACTIVE_COLORS[type.ordinal()]);
        }
    }

    public void setOffset(double i) {
        translate(getX() + i * this.offsetAxis.getX() * OFFSET_SCALING,
                getY() + i * this.offsetAxis.getY() * OFFSET_SCALING);
        if (background != null) {
            background.translate(background.getX() + i * this.offsetAxis.getX() * OFFSET_SCALING,
                    background.getY() + i * this.offsetAxis.getY() * OFFSET_SCALING);
        }
    }
}
