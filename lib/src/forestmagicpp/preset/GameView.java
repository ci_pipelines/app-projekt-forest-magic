package forestmagicpp.preset;

import forestmagicpp.preset.graphics.GQuality;
import forestmagicpp.preset.move.Move;

/**
 * Das GameView Interface wird fuer die Kommunikation mit dem Spielfenster verwendet. An keiner anderen Stelle als der
 * Instanziierung der Klasse {@link forestmagicpp.preset.customgraphics.GView} soll ein direkter Klassenverweis
 * verwendet werden. Kommunizieren Sie ausschliesslich ueber dieses Interface.
 */
public interface GameView {
    /**
     * Setzt den Titel des Spielfensters.
     *
     * @param title
     *         Titel
     */
    void setTitle(String title);

    /**
     * Setzt und uebergibt den Viewer des anzuzeigenden Spielbretts. Diese Methode muss aufgerufen werden, bevor Aufrufe
     * der Methoden {@link #request()} und {@link #update(Move)} erfolgen.
     *
     * @param viewer
     *         Viewer auf das anzuzeigende Spielbrett
     */
    void setViewer(Viewer viewer);

    /**
     * Erfragt einen interaktiven Spielzug ueber das Spielfenster. Ein Aufruf von {@link #request()} darf nur dann
     * erfolgen, wenn tatsaechlich ein Spielzug moeglich ist. Anderenfalls kommt es zu einem Deadlock!
     *
     * @return Interaktiv eingegebener Spielzug als {@link Move} Objekt
     */
    Move request();

    /**
     * Informiert das Spielfenster ueber einen im Spiel durchgefuehrten Zug. Wird {@link #update(Move)} nicht
     * aufgerufen, kann der neue Spielstand nicht angezeigt werden. Auch der Zug, der mit {@link #request()} von dem
     * Spielfenster erfragt wurde muss mit {@link #update(Move)} mitgeteilt werden, nachdem er im Hauptspiel
     * tatsaechlich ausgefuehrt wurde.
     *
     * @param move
     *         Der im Spiel neu ausgefuehrte Zug
     */
    void update(Move move);

    /**
     * Zeigt den Spielstatus im Spielfenster an.
     *
     * @param status
     *         Der Spielstatus als {@link GameStatus} Objekt
     */
    void showStatus(GameStatus status);

    /**
     * Aktiviert oder deaktiviert den Debug-Modus des Spielfensters. Hier koennen weitere Informationen zum Aufdecken
     * moeglicher Performanceprobleme angezeigt werden.
     *
     * @param enabled
     *         Ist true, wenn der Debug-Modus aktiviert werden soll
     */
    void showDebug(boolean enabled);

    /**
     * Aktiviert oder deaktiviert aktives Rendering. Siehe Projektbeschreibung fuer naehere Details.
     *
     * @param enabled
     *         Ist true, wenn das aktive Rendering aktiviert werden soll
     */
    void setActiveRendering(boolean enabled);

    /**
     * Setzt die Qualitaet, mit der das Spielfenster gezeichnet wird.
     *
     * @param quality
     *         Die Qualitaet als Wert von {@link GQuality}
     */
    void setQuality(GQuality quality);

    /**
     * Aktiviert oder deaktiviert das Pfad-Alignment. Siehe Projektbeschreibung fuer naehere Details.
     *
     * @param enabled
     *         Ist true, wenn das Pfad-Alignment aktiviert werden soll
     */
    void setPathAlignment(boolean enabled);
}
