package forestmagicpp.preset;

public enum PlayerType {
    HUMAN,
    RANDOM_AI,
    SIMPLE_AI
}
