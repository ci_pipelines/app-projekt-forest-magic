package forestmagicpp.preset;

public enum MoveStatus {
    OK,
    VERIFY,
    ILLEGAL
}
