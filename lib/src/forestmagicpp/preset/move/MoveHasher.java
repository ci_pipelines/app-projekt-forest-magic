package forestmagicpp.preset.move;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Random;

public final class MoveHasher {
    private Random random;
    private static int counter = 0;

    private MessageDigest digest;

    public MoveHasher() {
        random = new Random(System.nanoTime() * ++counter);
        try {
            digest = MessageDigest.getInstance("SHA-256");
        } catch (NoSuchAlgorithmException e) {
            System.err.println("SHA-256 unsupported! Exiting ...");
            e.printStackTrace();
            System.exit(1);
        }
    }

    public String getSalt() {
        return hash("" + random.nextInt() + random.nextInt() + random.nextInt());
    }

    public String calculateMoveHash(String salt, Move moveToHash) {
        return hash(salt + moveToHash);
    }

    private String hash(String unhashed) {
        byte[] encodedHash = digest.digest(unhashed.getBytes(StandardCharsets.UTF_8));
        return bytesToHex(encodedHash);
    }

    private static String bytesToHex(byte[] hash) {
        StringBuffer hexString = new StringBuffer();
        for (int i = 0; i < hash.length; i++) {
            String hex = Integer.toHexString(0xff & hash[i]);
            if (hex.length() == 1)
                hexString.append('0');
            hexString.append(hex);
        }
        return hexString.toString();
    }
}
