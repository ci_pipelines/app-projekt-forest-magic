package forestmagicpp.preset.move;

public abstract class AbstractMove implements PlayerMove {
    private static final String NO_HASH_LITERAL = "NO_HASH";

    private Move move;
    private String hash;

    protected AbstractMove(final Move move, final String hash) {
        setMove(move);
        setHash(hash);
    }

    @Override
    public Move getMove() {
        return move;
    }

    private void setMove(final Move move) {
        if (move == null) {
            throw new IllegalArgumentException("move == null");
        }
        this.move = move;
    }

    @Override
    public boolean hasHash() {
        return !getHash().equals(NO_HASH_LITERAL);
    }

    @Override
    public String getHash() {
        return hash;
    }

    private void setHash(final String hash) {
        if (hash == null) {
            this.hash = NO_HASH_LITERAL;
        } else {
            this.hash = hash;
        }
    }

    @Override
    public int hashCode() {
        return getMove().toString()
                        .hashCode() ^ getHash().hashCode();
    }

    @Override
    public boolean equals(final Object o) {
        if (o == null)
            return false;
        if (!(o instanceof AbstractMove))
            return false;
        AbstractMove m = (AbstractMove) o;
        return getMove().equals(m.getMove()) && getHash().equals(m.getHash());
    }

    @Override
    public String toString() {
        return getMove() + (hasHash() ? " [" + getHash() + "]" : "");
    }
}
