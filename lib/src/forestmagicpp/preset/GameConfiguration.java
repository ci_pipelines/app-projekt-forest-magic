package forestmagicpp.preset;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class GameConfiguration {
    private String board;
    private List<Position> startPositions;
    private int numberOfMouseRideUsages;
    private int numberOfCatRideUsages;
    private int numberOfOwlFlightUsages;
    private int numberOfSecretUsages;
    private Set<Integer> visibleRounds;
    private int maxNumberOfRounds;

    /**
     * Erzeugt eine Spielkonfiguration.
     *
     * @param data
     *         Enthaelt den gesamten Inhalt der Spielkonfigurationsdatei als String
     */
    public GameConfiguration(String data) {
        String[] lines = data.split("\n");
        this.board = lines[0];
        this.startPositions = parseStartPositions(lines[1]);
        String[] usages = lines[2].split(",");
        this.numberOfMouseRideUsages = Integer.parseInt(usages[0]);
        this.numberOfCatRideUsages = Integer.parseInt(usages[1]);
        this.numberOfOwlFlightUsages = Integer.parseInt(usages[2]);
        this.numberOfSecretUsages = Integer.parseInt(usages[3]);
        this.visibleRounds = new HashSet<>();
        String[] rawRounds = lines[3].split(",");
        for (String r : rawRounds) {
            this.visibleRounds.add(Integer.parseInt(r));
        }
        this.maxNumberOfRounds = Integer.parseInt(lines[4]);
    }

    private List<Position> parseStartPositions(String startPositions) {
        List<Position> positions = new ArrayList<>();
        String[] rawPositions = startPositions.split(",");
        for (String pos : rawPositions) {
            positions.add(Position.parsePosition(pos));
        }
        return positions;
    }

    public String getBoard() {
        return this.board;
    }

    public List<Position> getStartPositions() {
        return Collections.unmodifiableList(startPositions);
    }

    public int getNumberOfMouseRideUsages() {
        return numberOfMouseRideUsages;
    }

    public int getNumberOfCatRideUsages() {
        return numberOfCatRideUsages;
    }

    public int getNumberOfOwlFlightUsages() {
        return numberOfOwlFlightUsages;
    }

    public int getNumberOfSecretUsages() {
        return numberOfSecretUsages;
    }

    public Set<Integer> getVisibleRounds() {
        return Collections.unmodifiableSet(visibleRounds);
    }

    public int getMaxNumberOfRounds() {
        return maxNumberOfRounds;
    }
}
