package forestmagicpp.preset;

public class IllegalMoveException extends IllegalArgumentException {
    public IllegalMoveException(String msg) {
        super(msg);
    }

    public IllegalMoveException(String msg, Throwable e) {
        super(msg, e);
    }
}
