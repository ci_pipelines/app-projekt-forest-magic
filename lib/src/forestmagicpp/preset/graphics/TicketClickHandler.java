package forestmagicpp.preset.graphics;

import forestmagicpp.preset.PathType;

@FunctionalInterface
public interface TicketClickHandler {
    void handle(PathType type);
}
