package forestmagicpp.preset.graphics;

import forestmagicpp.preset.Position;

@FunctionalInterface
public interface FieldClickHandler {
    void handle(Position position);
}
