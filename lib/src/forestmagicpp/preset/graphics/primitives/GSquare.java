package forestmagicpp.preset.graphics.primitives;

import java.awt.geom.Rectangle2D;

public class GSquare extends GShape {
    public GSquare() {
        super(new Rectangle2D.Double(-0.5, -0.5, 1.0, 1.0));
    }
}
