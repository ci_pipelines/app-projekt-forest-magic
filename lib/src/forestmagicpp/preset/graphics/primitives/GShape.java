package forestmagicpp.preset.graphics.primitives;

import forestmagicpp.preset.graphics.GObject;

import java.awt.*;
import java.awt.geom.AffineTransform;

public class GShape extends GObject {
    private Shape shape;
    private Paint fillPaint, outlinePaint;
    private Stroke stroke;

    public GShape(final Shape shape) {
        if (shape == null) {
            throw new IllegalArgumentException("shape cannot be null");
        }
        this.shape = shape;
    }

    @Override
    protected void abstractDraw(Graphics2D g) {
        if (fillPaint != null) {
            g.setPaint(fillPaint);
            g.fill(shape);
        }
        if (outlinePaint != null) {
            if (stroke != null) {
                g.setStroke(stroke);
            }
            g.setPaint(outlinePaint);
            g.draw(shape);
        }
    }

    @Override
    public boolean contains(Point point) {
        AffineTransform inverse = getInverseGraphicsTransform();
        if (inverse == null) {
            return false;
        }
        return shape.contains(inverse.transform(point, null));
    }

    public void setShape(final Shape shape) {
        this.shape = shape;
    }

    public void setFillPaint(final Paint fillPaint) {
        this.fillPaint = fillPaint;
    }

    public void setStroke(final Stroke stroke) {
        this.stroke = stroke;
    }

    public void setOutlinePaint(final Paint outlinePaint) {
        this.outlinePaint = outlinePaint;
    }

    @Override
    public void update(float deltaTime) {
        // Do nothing per default
    }
}
