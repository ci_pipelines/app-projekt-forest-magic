package forestmagicpp.preset.graphics;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;

public class GContainer extends GObject {
    private List<GObject> children = new ArrayList<>();

    @Override
    protected synchronized void abstractDraw(Graphics2D g) {
        for (GObject go : children) {
            go.draw(g);
        }
    }

    @Override
    public synchronized boolean contains(Point point) {
        for (GObject go : children) {
            if (go.contains(point)) {
                return true;
            }
        }
        return false;
    }

    public synchronized void add(final GObject gObject) {
        children.add(gObject);
    }

    public synchronized void remove(final GObject gObject) {
        children.remove(gObject);
    }

    public synchronized int size() {
        return children.size();
    }

    public synchronized GObject get(int index) {
        return children.get(index);
    }

    public synchronized void clear() {
        children.clear();
    }

    @Override
    public synchronized void mouseOver(Point point, boolean value) {
        super.mouseOver(point, value);
        for (GObject go : children) {
            go.mouseOver(point, go.contains(point));
        }
    }

    @Override
    public synchronized void onMouseClick(Point point) {
        super.onMouseClick(point);
        for (GObject go : children) {
            if (go.contains(point)) {
                go.onMouseClick(point);
            }
        }
    }

    @Override
    public synchronized void onMousePress(Point point) {
        super.onMousePress(point);
        for (GObject go : children) {
            if (go.contains(point)) {
                go.onMousePress(point);
            }
        }
    }

    @Override
    public synchronized void onMouseRelease(Point point) {
        super.onMouseRelease(point);
        for (GObject go : children) {
            if (go.contains(point)) {
                go.onMouseRelease(point);
            }
        }
    }

    @Override
    public synchronized void update(float deltaTime) {
        for (GObject go : children) {
            go.update(deltaTime);
        }
    }
}
