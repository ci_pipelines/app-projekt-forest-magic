package forestmagicpp.preset.graphics;

public interface Updateable {
    void update(float deltaTime);
}
