package forestmagicpp.preset.graphics.input;

public enum KeyboardState {
    PRESSED,
    RELEASED
}
