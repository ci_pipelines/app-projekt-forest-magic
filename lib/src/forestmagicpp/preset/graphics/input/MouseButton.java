package forestmagicpp.preset.graphics.input;

public enum MouseButton {
    NONE,
    PRIMARY,
    SECONDARY,
    MIDDLE
}
