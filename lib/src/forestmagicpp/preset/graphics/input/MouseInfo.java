package forestmagicpp.preset.graphics.input;

import java.awt.*;

public class MouseInfo {
    private final Point point;
    private final MouseButton button;
    private final MouseState state;

    public MouseInfo(final Point point, final MouseButton button, final MouseState state) {
        this.point = point;
        this.button = button;
        this.state = state;
    }

    public Point getPoint() {
        return this.point;
    }

    public MouseButton getButton() {
        return this.button;
    }

    public MouseState getState() {
        return this.state;
    }
}
