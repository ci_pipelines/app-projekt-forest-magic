package forestmagicpp.preset;

public enum PathType {
    MOUSE_RIDE,
    CAT_RIDE,
    OWL_FLIGHT,
    SECRET_TUNNEL
}
